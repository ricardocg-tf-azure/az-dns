variable "private_zone" {
    type = string
    description = "The name of the DNS Zone. Must be a valid domain name."
}

variable "public_zone" {
    type = string
    description = "The name of the DNS Zone. Must be a valid domain name."
}