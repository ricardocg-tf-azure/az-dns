output "fqdn" {
    value = azurerm_dns_zone.example-public.fqdn
    description = "The fully qualified domain name of the Record Set."
}

output "fqdn_private" {
    value = azurerm_private_dns_zone.example-private.fqdn
    description = "The fully qualified domain name of the Record Set."
}